package com.arcarese.app;

public class BabysitterCalculator {

	// Constant Payrates per shift
	final int regularHourlyRate  = 12;
	final int bedtimeHourlyRate  = 8;
	final int midnightHourlyRate = 16;

	// Entry Point
	public static void main(String[] args) {

	}


	// ConvertTime() - Accepts HH:MM(AM/PM) date and returns an integer number value from 1 - 12
	public int convertTime(String inputTime) {

	String[] splitColon = inputTime.split(":");
	
	int hour = Integer.parseInt(splitColon[0]);
	
	if (hour <= 12 && hour >= 5) {
			return hour - 4;
		} else
			return hour + 8;
	}


	// isValidStartShift() - Validates start time.
	public boolean isValidStartShift(String startTime) {
		if (convertTime(startTime) < 12) { return true; }
		else { return false; }
	}


	// isValidEndShift() - Validates end time.
	public boolean isValidEndShift(String endTime, String startTime) {
		if (convertTime(endTime) > convertTime(startTime)) { return true; }
		else {return false; }
	}


	// isValidBedShift() - Validate bedtime shift.
	public boolean isValidBedShift(String bedTime, String startTime, String endTime) {
		if (convertTime(startTime) <= convertTime(bedTime) && convertTime(bedTime) <= convertTime(endTime)) { return true; }
		else { return false; }
	}


	// calcMidnightShiftHours() - Determine midnight shift hours based on end time.	
	public int calcMidnightShiftHours(String endTime) {

		if (convertTime(endTime) > convertTime("12:00AM")) {
			return convertTime(endTime) - convertTime("12:00AM");
		}
		else { return 0; }
	}

	public int calcBedtimeShiftHours(String startTime, String endTime, String bedTime) {

		if (convertTime(startTime) < convertTime("12:00AM")) {
			return convertTime("12:00") - convertTime(bedTime);
		} else
			return 0;
	}

	public int calcNormalShiftHours(String startTime, String endTime, String bedTime) {

		if (convertTime(bedTime) >= convertTime("12:00AM")) {
			return convertTime("12:00AM") - convertTime(startTime);
		} else if (convertTime(bedTime) < convertTime("12:00AM")) {
			return convertTime(bedTime) - convertTime(startTime);
		} else return 0;
	}

	public int calcTotalPay(String startTime, String bedTime, String endTime) {

		return (calcNormalShiftHours(startTime, endTime, bedTime) * regularHourlyRate)
				+ (calcBedtimeShiftHours(startTime, endTime, bedTime) * bedtimeHourlyRate)
				+ (calcMidnightShiftHours(endTime) * midnightHourlyRate);

	}
}