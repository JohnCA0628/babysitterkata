package com.arcarese.app;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *** Unit tests for babysitter kata App.
 **/
public class BabysitterCalculatorTest
{

	private BabysitterCalculator appTestObject;

	@Before
	public void executeBeforeEachTest() {
		appTestObject = new BabysitterCalculator();
	}

	// Test method to convert start time from string.
	@Test
	public void testConvertTimeString5PM() {
		assertEquals(1, appTestObject.convertTime("5:00PM"));
	}

	// Test method to convert end time from string.
	@Test
	public void testConvertTimeString4AM() {
		assertEquals(12, appTestObject.convertTime("4:00AM"));
	}

	// Test method to ensure that "5:00PM" is before the end time.
	@Test
	public void testConvertedStartTime5PMIsLessThanEndTime() {
		assertEquals(true, appTestObject.isValidStartShift ("5:00PM"));
	}

	// Test method to ensure "4:00AM" is before the end time. 
	@Test
	public void testConvertedStartTime4AMIsLessThanEndTime() {
		assertEquals(false, appTestObject.isValidStartShift("4:00AM"));
	}

	// Test method to ensure the end time 4:00AM is after start time 5:00PM.
	@Test
	public void testReturnTrueEndTimeIsAfterStartTime() {
		assertEquals(true, appTestObject.isValidEndShift("4:00AM", "5:00PM"));
	}

	// Test method to ensure that 7:00PM is not after 10:00PM.
	@Test
	public void testReturnFalseEndTime7pmIsAfterStartTime10pm() {
		assertEquals(false, appTestObject.isValidEndShift("7:00PM", "10:00PM"));
	}

	// Test method to validate that bedtime is within range of start time 5:00PM and end time 4:00AM.
	@Test
	public void testBedTimeIsWithinStartTimeAndEndTime() {
		assertEquals(true, appTestObject.isValidBedShift("11:00PM", "5:00PM", "4:00AM"));
	}

	// Test method to validate that bedtime shift is between two valid times.
	@Test
	public void testReturnFalseBedTime6pmIsWithinStartTime7pmEndTime11pm() {
		assertEquals(false, appTestObject.isValidBedShift("6:00PM", "7:00PM", "1:00PM"));
	}

	// Test method to validate the # of hours in the midnight shift.  As documented, midnight shift payrate supersedes other payrates.
	// Should return four hours for testing 4:00AM scenario.
	@Test
	public void testReturn4HoursFromMidnightShiftToEndShiftScenario1() {
		assertEquals(4, appTestObject.calcMidnightShiftHours("4:00AM"));
	}

	// Should return two hours for testing 2:00AM scenario.
	@Test
	public void testReturn2HoursFromMidnightToEndShiftScenario2() {
		assertEquals(2, appTestObject.calcMidnightShiftHours("2:00AM"));
	}

	@Test
	public void testReturn0HoursFromMidnightToEndTimeScenario1() {
		assertEquals(0, appTestObject.calcMidnightShiftHours("10:00PM"));
	}

	// Method to calculate # of hours in the bedtime shift.
	// Midnight shift hours payrate supersedes bedtime shift hours payrate, which supersedes regular shift hours payrate.
	//Scenario 1:
	// Bed Time: 6:00PM
	// Start Shift: 5:00PM
	// End Shift: 4:00AM
	@Test
	public void testReturnBedTimeScenario1() {
		assertEquals(5, appTestObject.calcBedtimeShiftHours("5:00PM", "4:00AM", "7:00PM"));  
	}

	//Scenario 2:
	// Bed Time: 10:00PM
	// Start Shift: 5:00PM
	// End Shift: 4:00AM
	@Test
	public void testReturnBedTimeScenario2() {
		assertEquals(2, appTestObject.calcBedtimeShiftHours("5:00PM", "4:00AM", "10:00PM"));  
	}

	//Scenario 3:
	// Bed Time: 10:00PM
	// Start Shift: 5:00PM
	// End Shift: 4:00AM
	@Test
	public void testReturnBedTimeScenario3() {
	assertEquals(2, appTestObject.calcBedtimeShiftHours("5:00PM", "4:00AM", "10:00PM"));	
	}

	// Method to calculate the # of hours in the normal shift.
	@Test
	public void testReturn7HoursNormalShift() {
		assertEquals(7, appTestObject.calcNormalShiftHours("5:00PM", "2:00AM", "4:00AM"));
	}

	@Test
	public void testReturn5HoursNormalShift() {
		assertEquals(5, appTestObject.calcNormalShiftHours("5:00PM", "4:00AM", "10:00PM"));
	}

	// Method to return total pay (fractional hours are unpaid).
	@Test
	public void testReturn136() {
		assertEquals(136, appTestObject.calcTotalPay("5:00PM", "9:00PM", "4:00AM"));
	}

	@Test
	public void testReturn112() {
		assertEquals(112, appTestObject.calcTotalPay("7:00PM", "9:00PM", "4:00AM"));
	}
}